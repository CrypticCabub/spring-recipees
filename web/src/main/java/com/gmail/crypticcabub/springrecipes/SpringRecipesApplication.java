package com.gmail.crypticcabub.springrecipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class SpringRecipesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRecipesApplication.class, args);
    }

}
