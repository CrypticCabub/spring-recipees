package com.gmail.crypticcabub.springrecipes.domain;

import org.hibernate.annotations.Type;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class RecipeIngredientId implements Serializable {

    @Type(type = "uuid-char")
    private UUID recipeId;
    @Type(type = "uuid-char")
    private UUID ingredientId;

    public UUID getRecipeId() {
        return recipeId;
    }

    public RecipeIngredientId() {
    }

    public RecipeIngredientId(UUID recipeId, UUID ingredientId) {
        this.recipeId = recipeId;
        this.ingredientId = ingredientId;
    }

    public RecipeIngredientId setRecipeId(UUID recipeId) {
        this.recipeId = recipeId;
        return this;
    }

    public UUID getIngredientId() {
        return ingredientId;
    }

    public RecipeIngredientId setIngredientId(UUID ingredientId) {
        this.ingredientId = ingredientId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecipeIngredientId that = (RecipeIngredientId) o;

        if (recipeId != null ? !recipeId.equals(that.recipeId) : that.recipeId != null) return false;
        return ingredientId != null ? ingredientId.equals(that.ingredientId) : that.ingredientId == null;
    }

    @Override
    public int hashCode() {
        int result = recipeId != null ? recipeId.hashCode() : 0;
        result = 31 * result + (ingredientId != null ? ingredientId.hashCode() : 0);
        return result;
    }
}
