package com.gmail.crypticcabub.springrecipes.domain;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Category extends AbstractPersistenceEntity {
    private String name;

    @OneToMany(mappedBy = "category")
    private Set<Recipe> recipes = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Recipe> getRecipes() {
        return recipes;
    }
}
