package com.gmail.crypticcabub.springrecipes.services;

import com.gmail.crypticcabub.springrecipes.domain.Recipe;
import com.gmail.crypticcabub.springrecipes.repositories.RecipeRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecipeService {
    private final RecipeRepository repository;

    public RecipeService(RecipeRepository repository) {
        this.repository = repository;
    }

    public List<Recipe> getAllRecipes(){
        List<Recipe> recipeList = new ArrayList<>();
        repository.findAll().forEach(recipeList::add);
        return recipeList;
    }
}
