package com.gmail.crypticcabub.springrecipes.domain;


import org.hibernate.annotations.Cascade;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;

@Entity
public class RecipeIngredient {

    @EmbeddedId
    private RecipeIngredientId id;
    @Version
    private Integer version;


    //using these takes over the id fields and causes hibernate to try to cascade persist them when this class is saved
    @ManyToOne
    @MapsId("recipeId")
    private Recipe recipe;


    @ManyToOne
    @MapsId("ingredientId")
    private Ingredient ingredient;


    private String ingredientSubNote;
    private String amount;

    public RecipeIngredient() {

    }

    public RecipeIngredient(RecipeIngredientId id) {
        this.id = id;
    }

    public RecipeIngredient(Recipe recipe, Ingredient ingredient) {
        this.id = new RecipeIngredientId(recipe.getId(), ingredient.getId());
        this.recipe = recipe;
        this.ingredient = ingredient;
    }

    public RecipeIngredientId getId() {
        return id;
    }

    @Deprecated
    public RecipeIngredient setId(RecipeIngredientId id) {
        this.id = id;
        return this;
    }

    public Integer getVersion() {
        return version;
    }

    @Deprecated
    public RecipeIngredient setVersion(Integer version) {
        this.version = version;
        return this;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public String getIngredientSubNote() {
        return ingredientSubNote;
    }

    public RecipeIngredient setIngredientSubNote(String ingredientSubNote) {
        this.ingredientSubNote = ingredientSubNote;
        return this;
    }

    public String getAmount() {
        return amount;
    }

    public RecipeIngredient setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecipeIngredient that = (RecipeIngredient) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


}
