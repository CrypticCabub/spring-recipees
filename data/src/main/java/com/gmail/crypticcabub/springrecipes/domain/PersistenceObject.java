package com.gmail.crypticcabub.springrecipes.domain;

import java.util.UUID;

public interface PersistenceObject {

    UUID getId();
    @Deprecated
    void setId(UUID uuid);

    Integer getVersion();
    @Deprecated
    void setVersion(Integer version);
}
