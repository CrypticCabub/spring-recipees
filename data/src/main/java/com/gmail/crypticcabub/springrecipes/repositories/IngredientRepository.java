package com.gmail.crypticcabub.springrecipes.repositories;

import com.gmail.crypticcabub.springrecipes.domain.Ingredient;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface IngredientRepository extends CrudRepository<Ingredient, UUID> {
    public Ingredient findByName(String name);
}
