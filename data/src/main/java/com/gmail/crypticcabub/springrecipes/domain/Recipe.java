package com.gmail.crypticcabub.springrecipes.domain;

import com.gmail.crypticcabub.springrecipes.repositories.RecipeIngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Recipe extends AbstractPersistenceEntity{

    private String name;

    @Lob
    private String description;
    private Duration prepTime;
    private Duration cookTime;
    private Integer servings;
    private String source;
    private String url;

    @Lob
    private String directions;

    @Enumerated(value = EnumType.STRING)
    private Difficulty difficulty;
    private Byte[] image;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "recipe")
    private Set<RecipeIngredient> ingredients = new HashSet<>();

    @ManyToOne
    private Category category;

    public RecipeIngredient addIngredient(Ingredient ingredient) {
        RecipeIngredient recipeIngredient = new RecipeIngredient(this, ingredient);
        ingredients.add(recipeIngredient);
        return recipeIngredient;
    }

    public String getName() {
        return name;
    }

    public Recipe setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Recipe setDescription(String description) {
        this.description = description;
        return this;
    }

    public Duration getPrepTime() {
        return prepTime;
    }

    public Recipe setPrepTime(Duration prepTime) {
        this.prepTime = prepTime;
        return this;
    }

    public Duration getCookTime() {
        return cookTime;
    }

    public Recipe setCookTime(Duration cookTime) {
        this.cookTime = cookTime;
        return this;
    }

    public Integer getServings() {
        return servings;
    }

    public Recipe setServings(Integer servings) {
        this.servings = servings;
        return this;
    }

    public String getSource() {
        return source;
    }

    public Recipe setSource(String source) {
        this.source = source;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Recipe setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getDirections() {
        return directions;
    }

    public Recipe setDirections(String directions) {
        this.directions = directions;
        return this;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public Recipe setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        return this;
    }

    public Byte[] getImage() {
        return image;
    }

    public Recipe setImage(Byte[] image) {
        this.image = image;
        return this;
    }

    public Set<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public Recipe setIngredients(Set<RecipeIngredient> ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    public Category getCategory() {
        return category;
    }

    public Recipe setCategory(Category category) {
        this.category = category;
        return this;
    }
}
