package com.gmail.crypticcabub.springrecipes.services;

import com.gmail.crypticcabub.springrecipes.domain.Ingredient;
import com.gmail.crypticcabub.springrecipes.repositories.IngredientRepository;
import org.springframework.stereotype.Service;


@Service
public class IngredientService {

    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Ingredient findOrCreateIngredient(String name) {
        var ingredient = ingredientRepository.findByName(name);
        if(ingredient == null) {
            ingredient = new Ingredient();
            ingredient.setName(name);
            ingredient = ingredientRepository.save(ingredient);
        }
        return ingredient;
    }


}
