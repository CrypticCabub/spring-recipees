package com.gmail.crypticcabub.springrecipes.bootstrap;

import com.gmail.crypticcabub.springrecipes.domain.Category;
import com.gmail.crypticcabub.springrecipes.domain.Ingredient;
import com.gmail.crypticcabub.springrecipes.domain.Recipe;
import com.gmail.crypticcabub.springrecipes.domain.RecipeIngredient;
import com.gmail.crypticcabub.springrecipes.repositories.CategoryRepository;
import com.gmail.crypticcabub.springrecipes.repositories.IngredientRepository;
import com.gmail.crypticcabub.springrecipes.repositories.RecipeIngredientRepository;
import com.gmail.crypticcabub.springrecipes.repositories.RecipeRepository;
import com.gmail.crypticcabub.springrecipes.services.IngredientService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.Duration;

@Component
public class InitialDataLoader implements CommandLineRunner {

    private final CategoryRepository categoryRepository;
    private final RecipeRepository recipeRepository;
    private final IngredientService ingredientService;
    private final IngredientRepository ingredientRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;

    public InitialDataLoader(CategoryRepository categoryRepository, RecipeRepository recipeRepository, IngredientService ingredientService, IngredientRepository ingredientRepository, RecipeIngredientRepository recipeIngredientRepository) {

        this.categoryRepository = categoryRepository;
        this.recipeRepository = recipeRepository;
        this.ingredientService = ingredientService;
        this.ingredientRepository = ingredientRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    @Transactional
    public void run(String... args) throws Exception {
        //if(true)return;
        if(recipeRepository.count() == 0) {
            loadCategories();
            loadRecipees();
        }
    }

    private void loadCategories() {
        Category cookies = new Category();
        cookies.setName("Cookies");
        categoryRepository.save(cookies);

        Category sams = new Category();
        sams.setName("Sams");
        categoryRepository.save(sams);

        System.out.println("Categories Loaded");
    }

    private void loadRecipees() {
        loadSugarCookies();
        loadSamsHogieRolls();
        System.out.println("Recipes Loaded");
    }

    public void loadSugarCookies() {
        Recipe recipe = new Recipe();

        recipe.setName("Best Sugar Cookies");
        recipe.setCategory(categoryRepository.findByName("Cookies").get());
        recipe.setUrl("https://sallysbakingaddiction.com/best-sugar-cookies/");
        recipe.setSource("SallysBakingAddiction.com");
        recipe.setCookTime(Duration.ofMinutes(12));
        recipe.setPrepTime(Duration.ofHours(2));
        recipe.setServings(24);

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Flour"))
                .setIngredientSubNote("All-Purpose")
                .setAmount("2 1/4 cups");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Baking Powder"))
                .setAmount("1/2 teaspoon");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Salt"))
                .setAmount("1/4 teaspoon");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Butter"))
                .setIngredientSubNote("Unsalted, Softened")
                .setAmount("3/4 Cup");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Sugar"))
                .setIngredientSubNote("Granulated")
                .setAmount("3/4 Cup");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Eggs"))
                .setIngredientSubNote("at room temperature")
                .setAmount("1");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Vanilla Extract"))
                .setAmount("2 teaspoons");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Almond Extract"))
                .setIngredientSubNote("optional")
                .setAmount("1/4 or 1/2 teaspoon");

        recipe.addIngredient(ingredientService.findOrCreateIngredient("Icing"))
                .setIngredientSubNote("preferably Royal or Easy Glaze Icing");




        recipe.setDescription("""
                With crisp edges, thick centers, and room for lots of decorating icing, \
                I know you’ll love these soft sugar cookies as much as I do. \
                The number of cookies this recipe yields depends on the size of the cookie cutter you use. \
                If you’d like to make dozens of cookies for a large crowd, double the recipe.
                """);
        recipe.setDirections("""
                1. Whisk the flour, baking powder, and salt together in a medium bowl. Set aside.
                
                2. In a large bowl using a hand mixer or a stand mixer fitted with a paddle attachment, \
                beat the butter and sugar together on high speed until completely smooth and creamy, about 2 minutes. \
                Add the egg, vanilla, and almond extract (if using) and beat on high speed until combined, about 1 minute. \
                Scrape down the sides and up the bottom of the bowl and beat again as needed to combine.
                
                3. Add the dry ingredients to the wet ingredients and mix on low until combined. \
                Dough will be relatively soft. If the dough seems too soft and sticky for rolling, \
                add 1 more Tablespoon of flour.
                
                4. Divide the dough into 2 equal parts. Place each portion onto a piece of lightly floured parchment paper \
                or a lightly floured silicone baking mat. With a lightly floured rolling pin, \
                roll the dough out to about 1/4-inch thickness. Use more flour if the dough seems too sticky. \
                The rolled-out dough can be any shape, as long as it is evenly 1/4-inch thick.
                
                5. Lightly dust one of the rolled-out doughs with flour. Place a piece of parchment on top. (This prevents sticking.) \
                Place the 2nd rolled-out dough on top. Cover with plastic wrap or aluminum foil, \
                then refrigerate for at least 1-2 hours and up to 2 days.
                
                6. Once chilled, preheat oven to 350°F (177°C). Line 2-3 large baking sheets with parchment paper \
                or silicone baking mats. Carefully remove the top dough piece from the refrigerator. \
                If it’s sticking to the bottom, run your hand under it to help remove it. \
                Using a cookie cutter, cut the dough into shapes. Re-roll the remaining dough \
                and continue cutting until all is used. Repeat with 2nd piece of dough. \
                (Note: It doesn’t seem like a lot of dough, but you get a lot of cookies from the dough scraps you re-roll.)
                
                7. Arrange cookies on baking sheets 3 inches apart. \
                Bake for 11-12 minutes or until lightly browned around the edges. \
                If your oven has hot spots, rotate the baking sheet halfway through bake time. \
                Allow cookies to cool on the baking sheet for 5 minutes \
                then transfer to a wire rack to cool completely before decorating.
                
                8.Decorate the cooled cookies with royal icing or easy glaze icing. \
                Feel free to tint either icing with gel food coloring. \
                No need to cover the decorated cookies as you wait for the icing to set. \
                If it’s helpful, decorate the cookies directly on a baking sheet \
                so you can stick the entire baking sheet in the refrigerator to help speed up the icing setting.
                
                9. Enjoy cookies right away or wait until the icing sets to serve them. \
                Once the icing has set, these cookies are great for gifting or for sending. \
                Plain or decorated cookies stay soft for about 5 days when covered tightly at room temperature. \
                For longer storage, cover and refrigerate for up to 10 days.
                """);




        recipeRepository.save(recipe);
    }

    private void loadSamsHogieRolls() {
        Recipe recipe = new Recipe();
        recipe.setCategory(categoryRepository.findByName("Sams").get());
        recipe.setName("Sames Hogie Rolls");
        recipe.setDirections("""
                1. Defrost Dough in fridge 15-24 hours (18hrs optimal)
                
                2. Proof for ~45 mins (until dough doubles in size) -- mist with water as needed to keep from drying out
                
                3. Score with diagonal cuts to allow bread to rise (1 for small, 2-3 for medium)
                
                4. Bake at 350 degrees for 15-18 minutes or until golden
                """);

        recipeRepository.save(recipe);
    }
}
