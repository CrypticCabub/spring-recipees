package com.gmail.crypticcabub.springrecipes.domain;

import com.gmail.crypticcabub.springrecipes.domain.uuid.IdGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.util.UUID;

//https://web.archive.org/web/20170710134607/http://www.onjava.com/pub/a/onjava/2006/09/13/dont-let-hibernate-steal-your-identity.html
@MappedSuperclass
public abstract class AbstractPersistenceEntity implements PersistenceObject {

    @Id
    @Type(type = "uuid-char")
    private UUID id = IdGenerator.createID();

    @Version
    private Integer version;

    public UUID getId() {
        return id;
    }

    @Deprecated
    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    @Deprecated
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractPersistenceEntity that = (AbstractPersistenceEntity) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
