package com.gmail.crypticcabub.springrecipes.repositories;

import com.gmail.crypticcabub.springrecipes.domain.Category;
import com.gmail.crypticcabub.springrecipes.domain.Ingredient;
import com.gmail.crypticcabub.springrecipes.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;
import java.util.UUID;

public interface RecipeRepository extends CrudRepository<Recipe, UUID> {
    Set<Recipe> findByCategory(Category category);
    Set<Recipe> findByIngredientsContaining(Ingredient ingredient);
}
