package com.gmail.crypticcabub.springrecipes.repositories;

import com.gmail.crypticcabub.springrecipes.domain.RecipeIngredient;
import com.gmail.crypticcabub.springrecipes.domain.RecipeIngredientId;
import org.springframework.data.repository.CrudRepository;

public interface RecipeIngredientRepository extends CrudRepository<RecipeIngredient, RecipeIngredientId> {
}
