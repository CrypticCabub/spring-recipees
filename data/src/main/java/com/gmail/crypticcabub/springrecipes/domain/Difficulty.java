package com.gmail.crypticcabub.springrecipes.domain;

public enum Difficulty {
    EASY, MEDIUM, HARD
}
