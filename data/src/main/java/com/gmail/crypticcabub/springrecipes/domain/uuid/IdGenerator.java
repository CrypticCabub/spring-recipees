package com.gmail.crypticcabub.springrecipes.domain.uuid;

import java.util.UUID;

public class IdGenerator {
    public static UUID createID() {
        return UUID.randomUUID();
    }
}
